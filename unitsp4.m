function [c,ceq, DC, DCeq] = unitsp4(x, p)
c   = [];
ceq=zeros(1,p);
for i=1:p
    ceq(i) = x(4*i-3)^2 + x(4*i-2)^2 + x(4*i-1)^2 + x(4*i)^2 - 1;
end
DC= [];
DCeq = zeros(4*p, p);
for i=1:p
    DCeq(4*i-3:4*i, i)=[2*x(4*i-3); 2*x(4*i-2); 2*x(4*i-1); 2*x(4*i)];
end
