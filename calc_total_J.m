function total_J = calc_total_J(N, qa_his, qo_his, qu_his, Ts, Qa, Qo, Qu, Terminalcost)
    total_J=zeros(1,N+1);
    for k=1:N
        qa_k    = qa_his(1:4,k+1);
        qo_k    = qo_his(1:4,k+1);
        qu_km   = qu_his(1:4, k);
        qakqi   = qa_k - [1;0;0;0];
        qokqi   = qo_k - [1;0;0;0];
        qukqi   = qu_km- [1;0;0;0];
        if k==N
            Qa=Terminalcost*Qa;
        end
        Jk=Ts*0.5*( qakqi'*Qa*qakqi + qokqi'*Qo*qokqi + qukqi'*Qu*qukqi );
        total_J(k+1)=total_J(k)+Jk;
    end
end