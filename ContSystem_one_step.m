function [qa_kp, qo_kp] = ContSystem_one_step(qa_k, qo_k, qu_k, Ts)
    x_0=[qa_k; qo_k];
    [~,x_kp] = ode45(@(t,x)ContSystem(x,qu_k, Ts),[0 Ts],x_0);
    qa_kp = x_kp(end, 1:4);
    qo_kp = x_kp(end, 5:8);
end


function [dx] = ContSystem(x,qu, Ts)
    qa_h = x(1:4);
    qo_h = x(5:8);
    omega_h = qomega_to_omega(qo_h, Ts);
    u_h = qomega_to_omega(qu, Ts);
    dqa = 0.5 * q_product(qa_h, [0; omega_h]);
    dqo = 0.5 * q_product(qo_h, [0; u_h]);
    dx = [dqa; dqo];
end