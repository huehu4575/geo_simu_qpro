function [bar_qa, bar_qo] = calc_state_p_step(qa_k, qo_k, bar_qu, Ts, p)
    % LET: bar_*=zeros(size,1)
    bar_qa=zeros(4*p,1);  % qa_{k+1}, ..., qa_{k+p}
    bar_qo=zeros(4*p,1);  % qo_{k+1}, ..., qo_{k+p}

    % Calculate *_{k+1} from *_k
    bar_qa(1:4) = q_product(qa_k, qo_k);
    bar_qo(1:4) = q_product(qo_k, bar_qu(1:4));
    for i=2:p
        bar_qa(4*i-3:4*i) = q_product( bar_qa( 4*(i-1)-3:4*(i-1) ), bar_qo( 4*(i-1)-3:4*(i-1)) );
        bar_qo(4*i-3:4*i) = q_product( bar_qo( 4*(i-1)-3:4*(i-1) ), bar_qu( 4*i-3:4*i ) );
    end
end