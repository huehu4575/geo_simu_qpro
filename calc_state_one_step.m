function [qa_kp, qo_kp] = calc_state_one_step(qa_k, qo_k, qu_k, Ts)
    qa_kp=zeros(4,1);  % qa_k+1
    qo_kp=zeros(4,1);  % qo_k+1

    % Calculate *_{k+1} from *_k
    qo_kp(1:4) = q_product(qo_k, qu_k);         % qo_{k+1} = qo_k (X) qu_k 
    qa_kp(1:4) = q_product(qa_k, qo_k);         % qa_{k+1} = qa_k (X) qo_k
end