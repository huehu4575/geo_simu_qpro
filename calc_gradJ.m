function gradJ = calc_gradJ(bar_qa, bar_qo, bar_qu, qo_k, barQa, barQo, barQu, p, breve_qa, breve_qo, breve_qu)

    %% calc A_2, A_3, ..., A_{N-1} & B_1, B_2, ..., B_{N-1}
    A=repmat(eye(4), 1,p);  % A = [A_0=I, A_1(qo_1), A_2(qo_2), ..., A_{N-1}(qa_{N-1})]
    B=repmat(eye(4), 1,p);  % B = [B_0=I, B_1(qa_1), B_2(qa_2), ..., B_{N-1}(qa_{N-1})]
    
    for i = 1:(p-1) % calc A_2 , ..., A_{N-1} & B_*
        qo_i = bar_qo(4*i-3:4*i);
        qa_i = bar_qa(4*i-3:4*i);
        A(1:4, 4*i+1:4*(i+1)) = calc_Hmk(qo_i);
        B(1:4, 4*i+1:4*(i+1)) = calc_Hpk(qa_i);
    end
    %A(1:4, 1:4)=eye(4);  % A_1 = I4
    
    %% calc C_2, C_3, ..., C_{N-1} & D_1, D_2, ..., D_{N-1}
    C=zeros(4, 4*p);  % C  = [C_0(qu_0), C_1(qu_1), ..., C_{N-1}(qo_{N-1})]
    D=zeros(4, 4*p);  % D  = [D_0(qo_0), D_1(qo_1), ..., D_{N-1}(qo_{N-1})]
    for i = 0:(p-1) % calc C_2 , ..., C_{N-1} & D_*
        qu_i = bar_qu(4*i+1:4*(i+1));
        if i==0
            qo_i = qo_k;
        else
            qo_i = bar_qo(4*(i-1)+1:4*i);
        end
        C(1:4, 4*i+1:4*(i+1)) = calc_Hmk(qu_i);
        D(1:4, 4*i+1:4*(i+1)) = calc_Hpk(qo_i);
    end
    
    dado = zeros(4*p, 4*p);
    dodu = zeros(4*p, 4*p);
    
    for j=1:(p-1)
       i=j+1;
       dado(4*i-3:4*i, 4*j-3:4*j)=get_Mk(B,j);
       for i=(j+2):p
           dado(4*i-3:4*i, 4*j-3:4*j)=get_Mk(A,i-1)*dado(4*(i-1)-3:4*(i-1), 4*j-3:4*j);      
       end
    end
    for j=1:p
       i=j;
       dodu(4*i-3:4*i, 4*j-3:4*j)=get_Mk(D, j-1);
       for i=(j+1):p
           dodu(4*i-3:4*i, 4*j-3:4*j)=get_Mk(C,i-1)*dodu(4*(i-1)-3:4*(i-1), 4*j-3:4*j);       
       end
    end

%     %% calc A_2, A_3, ..., A_{N-1} & B_1, B_2, ..., B_{N-1}
%     A=zeros(4, 4*(p-1));  % A  = [A_1(qo_1), A_2(qo_2), ..., A_{N-1}(qa_{N-1})]
%     B=zeros(4, 4*(p-1));  % B  = [B_1(qa_1), B_2(qa_2), ..., B_{N-1}(qa_{N-1})]
%     for i = 1:(p-1) % calc A_2 , ..., A_{N-1} & B_*
%         qo_i = bar_qo(4*i-3:4*i);
%         qa_i = bar_qa(4*i-3:4*i);
%         A(1:4, 4*i-3:4*i) = calc_Hmk(qo_i);
%         B(1:4, 4*i-3:4*i) = calc_Hpk(qa_i);
%     end
%     %A(1:4, 1:4)=eye(4);  % A_1 = I4
%     
%     %% calc C_2, C_3, ..., C_{N-1} & D_1, D_2, ..., D_{N-1}
%     C=zeros(4, 4*p);  % C  = [C_0(qu_0), C_1(qu_1), ..., C_{N-1}(qo_{N-1})]
%     D=zeros(4, 4*p);  % D  = [D_0(qo_0), D_1(qo_1), ..., D_{N-1}(qo_{N-1})]
%     for i = 0:(p-1) % calc C_2 , ..., C_{N-1} & D_*
%         qu_i = bar_qu(4*i+1:4*i+4);
%         if i==0
%             qo_i = qo_k;
%         else
%             qo_i = bar_qo(4*(i-1)+1:4*(i-1)+4);
%         end
%         C(1:4, 4*i+1:4*i+4) = calc_Hmk(qu_i);
%         D(1:4, 4*i+1:4*i+4) = calc_Hpk(qo_i);
%     end
%     
%     dado = zeros(4*p, 4*p);
%     dodu = zeros(4*p, 4*p);
%     
% 
%     for j=1:(p-1)
%        i=j+1;
%        dado(4*i-3:4*i, 4*j-3:4*j)=B(1:4, 4*j-3:4*j);
%        for i=(j+2):p
%            dado(4*i-3:4*i, 4*j-3:4*j)=A(1:4, 4*(i-1)-3:4*(i-1))*dado(4*(i-1)-3:4*(i-1), 4*j-3:4*j);      
%        end
%     end
%     for j=1:p
%        i=j;
%        dodu(4*i-3:4*i, 4*j-3:4*j)=D(1:4, 4*j-3:4*j);
%        for i=j+1:p
%            dodu(4*i-3:4*i, 4*j-3:4*j)=C(1:4, 4*i-3:4*i)*dodu(4*(i-1)-3:4*(i-1), 4*j-3:4*j);       
%        end
%     end
    g1=breve_qa'*barQa*dado*dodu;
    g2=breve_qo'*barQo*dodu;
    g3=breve_qu'*barQu;
    gradJ = g1+g2+g3;
end