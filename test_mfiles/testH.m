clear all
close all
T=0.01;
om=[-0.01; 0; 0];
d=[0.001; 0; 0];
t=-0.01:0.001:0.01;
[a,b]=size(t);
H_his=[]
for i=1:b
   H=eye(3);
   omega=om+d*i;
   no=norm(omega);
   w=no*T/2;
   for n=1:3
       for m=1:3
           delta=0;
           if n==m
               delta=1;
           end
            hnm=omega(n)*omega(m)/no/no*T/2*cos(w)+(delta/no-omega(n)*omega(m)/no/no/no)*sin(w)
            H(n,m)=hnm;
       end
   end
   no
   H
   H_his=[H_his H];
end
%plot(t, g_his)