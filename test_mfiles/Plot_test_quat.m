clear all;
close all;
addpath('./func','./test_mfiles')
R0 = rot_y(-pi/6)*rot_z(pi/4);
R0 = eye(3);
q0 = SO3_to_q(R0);
q0.z;
q0.v;
om0= [0,0,0];

global Ts
Ts = 0.1;

R_his  = [R0];
qz_his = [q0.z];
qv_his = [q0.v];
om_his = [om0];
u=[-0.5,1,0.1];
for k=0:100
    qk.z=qz_his(end);
    qk.v=qv_his(end-2:end);
    omk=om_his(end,1:3);
    qom=omega_to_qomega(omk)
    qkp  = q_product(qk, qom);
    omkp = omk+u*Ts;
    

    qz_his = [qz_his; qkp.z];
    qv_his = [qv_his; qkp.v];
    %[theta,xi]=q_to_theta_xi(qkp);
    %hxi=omega_to_omegahat(xi)
    %Rkp=expm(hxi*theta);
    Rkp = q_to_SO3(qkp);
    R_his = [R_his; Rkp];
    om_his= [om_his; omkp];
end

figure(1);
clf;
hcube = plotcube([1 1.5 0.2], [0 0 0], 0.8,[1,0,0]);
ccube = plotcube([0.05 0.05 0.05], [0 0 0], 1.0,[0,0,1]);
view(3);
axis equal;
%axis([-3 3 -3 3 -3 3]);
axis([-2 2 -2 2 -2 2]);
axis vis3d;
grid on;
ax = gca;
t = hgtransform('Parent',ax);
cellfun(@(h)set(h,'Parent',t),hcube)

f1=figure(1);
clf;
view(3);
axis equal;
%axis([-3 3 -3 3 -3 3]);
axis([-2 2 -2 2 -2 2]);
axis vis3d;
grid on;
yticks([-3:1:3])
zticks([-3:1:3])
ax = gca;

n = 100;
for i=1:n
    hcubei{i} = plotcube([1 1.5 0.2], [0 0 0], 0.2);
    ti{i} = hgtransform('Parent',gca);
    cellfun(@(h)set(h,'Parent',ti{i}),hcubei{i});
end
hcube = plotcube([1 1.5 0.2], [0 0 0], 0.8);
ccube = plotcube([0.05 0.05 0.05], [0 0 0], 1.0,[0,0,1]);
t = hgtransform('Parent',ax);
cellfun(@(h)set(h,'Parent',t),hcube);

filename = 'movieq';
mov = VideoWriter([filename '.avi'], 'Motion JPEG AVI');
mov.FrameRate = 1/Ts;
open(mov);
warning('off','MATLAB:hg:DiceyTransformMatrix')
for k=1:n
    t.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
    %ti{k}.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];

    drawnow;
    frame=getframe(f1);
    writeVideo(mov,frame);
    %pause
end
close(mov);