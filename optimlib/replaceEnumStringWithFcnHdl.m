function value = replaceEnumStringWithFcnHdl(solver, optionName, value)
%REPLACEENUMSTRINGWITHFCNHDL Replace enumerated string with fcn handle
%
%   OUT = REPLACEENUMSTRINGWITHFCNHDL(SOLVER, OPTION, VALUE) replaces the string
%   IN with a function handle if it matches one of the strings in
%   POSSSTRINGS.
%
%   OUT = REPLACEENUMSTRINGWITHFCNHDL(IN, POSSSTRINGS) replaces any
%   instance of POSSSTRING in the cell array IN with a function handle.

%   Copyright 2017 The MathWorks, Inc.

if ischar(value) || (isstring(value) && isscalar(value))
    options = optim.options.(solver);
    possStrings = options.PropertyMetaInfo.(optionName)(1).Values;
    if any(strcmp(value, possStrings))
        value = str2func(value);
    end
end

