function msg = qpTrustRegionMsg(id,anchor,printNow,solverName)
%

%qpTrustRegionMsg Produces exit messages for QP trust-region-reflective
%algorithm.
%
% Helper function for the trust-region-reflective algorithms of quadprog
% and lslin.
%

%   Copyright 2018 The MathWorks, Inc.


% Prep strings for message holes
if ~isempty(solverName)
    msgHoles = {'','',solverName};
else
    msgHoles = {'',''};
end

% First, get message for the output structure, without links
msg = getString(message(id,msgHoles{:}));
if printNow
    % If printing to the command window, get the message again
    [~,msgHoles{1},msgHoles{2}] = addLink('','optim','msg_csh\optim_msg_csh.map',anchor,true);
    disp([newline getString(message(id,msgHoles{:}))]);
end