function [pvec,LZ,UZ,pcolZ,PZ,PT]  = project(A,vec,PT,LZ,UZ,pcolZ,PZ)
%PROJECT Non-orthogonal projector
%
% pvec = PROJECT(A,vec) is a non-orthogonal projection of vec
% onto null(A).
%
% pvec = PROJECT(A,vec,PT,LZ,UZ,pcolZ,PZ) is a non-orthogonal 
% projection of vec
% onto null(A). PT is a permutation matrix such that A*PT 
% has a leading nonsingular  m-by-m matrix, A_1,
% where m is the number of rows of A
% PZ*A_1(:,pcolZ) = LZ*UZ, the sparse LU-factorization on A_1.
%	
% [pvec,LZ,UZ,pcolZ,PZ,PT]  = project(A,vec) is a non-orthogonal 
% projection of
% vec onto null(A). PT is a permutation matrix such that A*PT
% has a leading nonsingular  m-by-m matrix, A_1,
% where m is the number of rows of A
% PZ*A_1(:,pcolZ) = LZ*UZ, the sparse LU-factorization on A_1.

%   Copyright 1990-2017 The MathWorks, Inc.

[m,n] = size(A); 
if m > n
   error(message('optimlib:project:InvalidA')); 
end
if nargin < 3 || isempty(PT)
   PT = findp(A); 
end

A = A*PT'; 
vec = PT*vec;

% Disable warnings about singular matrices thrown from backslash (called in
% fzmult()). 
% NOTE: this is not done in fzmult since it is called in tight loops
% elsewhere.
warningstate1 = warning('off', 'MATLAB:nearlySingularMatrix');
warningstate2 = warning('off', 'MATLAB:singularMatrix');

doTranspose = true;
noTranspose = false;
if nargin < 6
   [pvec,LZ,UZ,pcolZ,PZ] = fzmult(A,vec,doTranspose);
else
   pvec = fzmult(A,vec,doTranspose,LZ,UZ,pcolZ,PZ);
end
pvec = PT'*fzmult(A,pvec,noTranspose,LZ,UZ,pcolZ,PZ);

warning([warningstate1 warningstate2]);




