function solvers = allSolvers

% ALLSOLVERS  returns a cell array of Strings. Each string is the name of an 
%             optimoptions classType (className)

%     Copyright 2017 The MathWorks, Inc.

mc = metaclass(optim.options.Fmincon);
cp = mc.ContainingPackage;
allClasses = cp.ClassList;
numClasses = length(allClasses);
solvers = cell(1, numClasses);
isAbstract = [allClasses.Abstract];
[solvers{:}] = deal(allClasses.Name);
solvers = solvers(~isAbstract);

solvers = setdiff(solvers, 'optim.options.OptionAliasStore');