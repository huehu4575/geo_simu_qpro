function [jsonMat,jsonString] = autoTab(classAlgName)
% AUTOTAB  Generates the .json file for tab completion for one solver
%   The file takes a solver from allSolvers.m to produce a .json file like 
%   functionSignatures.json. The file masterAutoTab.m uses autoTab.m to
%   generate the complete .json file.

% Arguments
% class2change = eval(classAlgName);
% optsStore = getOptionsStore(class2change);

%  
%     Copyright 2017-2018 The MathWorks, Inc.

className = strip(erase(classAlgName,"optim.options."));

propInfo = optim.options.(className).PropertyMetaInfo;

% Create json data
propList = string(sort(fieldnames(propInfo)));
numProps = numel(propList);

% Pre-allocate string array
jsonMat = {};

% First argument is always the solver name
jsonMat{1,1} = "solver";
jsonMat{1,2} = "required";

% Atter that are the N-V pairs
jsonMat(2:numProps+1,2)= {"namevalue"};

% Remove "Options" from end of class name
solverName = lower(strip(erase(className,"Options")));

% Build the "choices" list in JSON
jsonMat{1,3} = {{"choices={'" + solverName + "'}"}, {classAlgName}};

for i = 1:numProps
    jsonMat{i+1,1} = propList(i);
    thisType = {propInfo.(char(propList(i))).Type};
    % test if Type is a string or a cell
    if (length(thisType) == 1 && strcmp(thisType{:},'enum'))
        jsonMat{i+1,3} = "choices=" + classAlgName + ".PropertyMetaInfo." + propList(i) + ".Values";
    elseif (length(thisType) == 1 && strcmp(thisType{:},'file'))
        filetypes = optim.options.(className).PropertyMetaInfo.(propList(i)).Values;
        % Format the list of filetypes like "*.csv,*.xls,*.txt"
        filetypes = join("*." + filetypes,",");
        % MAT-files get a little extra
        if strcmp(filetypes,'*.mat')
            jsonMat{i+1,3} = {{'file=*.mat'},{'matlabpathfile=*.mat'},{'char'}};
        else
            jsonMat{i+1,3} = "file=" + filetypes;
        end
    else
       % Non-enum types are just listed as is
       % TODO: we can add more checking in the JSON file (e.g.
       % non-negativity, integrality, etc.) We can also add this in the
       % options class metadata.
       jsonMat{i+1,3} = thisType;
    end
end

% Convert data into json format
titles = {'name','kind','type'};
jsonStruct = cell2struct(jsonMat,titles,2);
jsonString = jsonencode(struct('optimoptions',struct('inputs',jsonStruct)));
