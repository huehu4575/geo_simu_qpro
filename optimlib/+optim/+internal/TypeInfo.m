classdef TypeInfo
    
     %
    %   TypeInfo
    %
    %   This class defines structures for objects to be used in tab
    %   completion
    
    %   Copyright 2017 The MathWorks, Inc.
    
    methods (Static, Access=public)
        
        function metaInfo = logicalType()
            metaInfo = struct('Type',{'logical', 'scalar'},'Name','logical');
        end

        function metaInfo = integerType()
            metaInfo = struct('Type','integer','Name','integer');
        end

        function metaInfo = numericType()
            metaInfo = struct('Type','numeric','Name','numeric');
        end
        
        function metaInfo = fcnType()
            metaInfo = struct('Type','function_handle','Name','fcn');
        end
        
        function metaInfo = fcnOrEmptyType()
            metaInfo = struct('Type',{ {'function_handle'}, {'@(x) isempty(x)'}}, 'Name', 'fcnorempty');
        end
        
        function metaInfo = fcnEnumType(values)
            validStrings = values;
            values = cellfun(@(x) ['''', x, ''''], values, 'UniformOutput', false);
            str = "choices= {" + strjoin(values, ',') + "}";
            metaInfo = struct('Type',{{str}, {'function_handle'}, {'@(x) isempty(x)'}}, ...
                'Values', {validStrings}, 'Name', 'fcnenum');
        end

        function metaInfo = enumType(values)
            metaInfo = struct('Type','enum','Values',{values},'Name','enum');
        end
        
        function metaInfo = positiveNumericType()
            metaInfo = struct('Type',{'numeric','>=0'},'Name','positivenumeric');
        end
        
        function metaInfo = nonNegIntegerType()
            metaInfo = struct('Type',{'integer','>=0'},'Name','nonneginteger');
        end
        
        function metaInfo = positiveIntegerType()
            metaInfo = struct('Type',{'integer','>0'},'Name','positiveinteger');
        end
       
        function metaInfo = fileType(filetypes)
            % TODO: desired JSON output should look similar to:
            % {"name":"OptionName", "kind":"namevalue", "type":[["file=*.mat"], ["matlabpathfile=*.mat"], ["char"]]},
            
            metaInfo = struct('Type','file','Values',filetypes,'Name','file');
        end        
    end
end

