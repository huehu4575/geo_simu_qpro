function FinDiffRelStep = getDefaultFinDiffRelStep(FinDiffType)
%GETDEFAULTFINDIFFRELSTEP Get the default for FinDiffRelStep
%
%   FINDIFFRELSTEP = GETDEFAULTFINDIFFRELSTEP(FINDIFFTYPE) returns the
%   FinDiffRelStep default value given the finite difference type.

%   Copyright 2013 The MathWorks, Inc.

switch FinDiffType
    case 'forward'
        FinDiffRelStep = 'sqrt(eps)';
    case 'central'
        FinDiffRelStep = 'eps^(1/3)';
end
