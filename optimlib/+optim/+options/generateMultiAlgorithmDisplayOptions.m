function OS = generateMultiAlgorithmDisplayOptions(OS, solverName)
%GENERATEMULTIALGORITHMDISPLAYOPTIONS Display metadata for multialgorithm
%                                     options
%
%   OS = GENERATEMULTIALGORITHMDISPLAYOPTIONS(OS, SOLVER) creates the
%   DisplayOptions and NumDisplayOptions metadata in the OptionsStore
%   structure. DisplayOptions is a 1-by-numAlg cell array;
%   DisplayOptions{i} is a cellstr containing the options that should be
%   displayed for the i-th algorithm. NumDisplayOptions(i) is the number of
%   options displayed for the i-th algorithm, i.e. numel(DisplayOptions{i)

%   Copyright 2015-2016 The MathWorks, Inc.

% Get the meta-class for the solver options and all the property names
mc = meta.class.fromName(solverName);
numProps = length(mc.PropertyList);
allPropNames = cell(numProps, 1);
[allPropNames{:}] = deal(mc.PropertyList.Name);

% Initialize DisplayOptions and NumDisplayOptions
numAlgs = length(OS.AlgorithmDefaults);
OS.NumDisplayOptions = zeros(1, numAlgs);
OS.DisplayOptions = cell(1, numAlgs);

% Set DisplayOptions and NumDisplayOptions for each algorithm
for i = 1:numAlgs
    
    % DisplayOptions{i} is a cellstr containing names of the options to be
    % displayed for this algorithm
    OS.DisplayOptions{i} = cell(1, 0);
    
    % Get all the algorithm options in the original (i.e. pre-alias, e.g.
    % TolX) format.
    thisAlgOptions = fieldnames(OS.AlgorithmDefaults{i});
    
    % Loop over original algorithm options to find those that should be
    % displayed
    for j = 1:length(thisAlgOptions)
        
        % Get possible options from j-th original algorithm option
        thisName = optim.options.OptionAliasStore.getNameFromAlias(thisAlgOptions{j});
        
        % Loop over options that alias to the j-th original algorithm
        % option and append any options that should be displayed.
        for k = 1:length(thisName)
            if isDisplayOption(mc, thisName{k}, allPropNames)
                OS.DisplayOptions{i}(end+1) = {thisName{k}};
            end
        end
    end

    % Add the Algorithm option
    OS.DisplayOptions{i}{end+1} = 'Algorithm';
    
    OS.NumDisplayOptions(i) = length(OS.DisplayOptions{i});
end

function displayThisOption = isDisplayOption(mc, thisName, allPropNames)

% For options that alias to an original one, see if the option
% exists and is not hidden

idxName = strcmp(allPropNames, thisName);
thisProp = mc.PropertyList(idxName);
displayThisOption = ~isempty(thisProp) && ~thisProp.Hidden;