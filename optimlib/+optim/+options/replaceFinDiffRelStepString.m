function finDiffRelStep = replaceFinDiffRelStepString(finDiffRelStep)
%REPLACEFINDIFFRELSTEPSTRING Replace string value in FinDiffRelStep
%
%   OPTIONS = REPLACEFINDIFFRELSTEPSTRING(OPTIONS) replaces a string value
%   in FinDiffRelStep with the equivalent numerical value.

%   Copyright 2013-2017 The MathWorks, Inc.

if ischar(finDiffRelStep)
    % At this point we know that FinDiffRelStep will either be
    % 'sqrt(eps)' or 'eps^(1/3)'.
    % If the string stats with 's', we know it's the former.
    if strncmp(finDiffRelStep, 's',1)
        finDiffRelStep = sqrt(eps);
    else
        finDiffRelStep = eps^(1/3);
    end
end
