function optionsStore = getDisplayOptionFieldsFor16a(optionsStore, currentOptionsStore)
%GETDISPLAYOPTIONFIELDSFOR16A Get the display option fields if loading a
%                             an old object into 16a

%   Copyright 2016 The MathWorks, Inc.

% Get the display options
optionsStore.NumDisplayOptions = currentOptionsStore.NumDisplayOptions;
optionsStore.DisplayOptions = currentOptionsStore.DisplayOptions;