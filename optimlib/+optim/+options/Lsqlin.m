classdef (Sealed) Lsqlin < optim.options.MultiAlgorithm
%

%Lsqlin Options for LSQLIN
%
%   The OPTIM.OPTIONS.LSQLIN class allows the user to create a set of
%   options for the LSQLIN solver. For a list of options that can be set,
%   see the documentation for LSQLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN creates a set of options for LSQLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(PARAM, VAL, ...) creates a set of options
%   for LSQLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS   

%   Copyright 2012-2017 The MathWorks, Inc.    
    
    properties (Dependent)
%CONSTAINTTOLERANCE Tolerance on the constraint violation
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        ConstraintTolerance    
        
%DISPLAY Level of display
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        Display

%FUNCTIONTOLERANCE Termination tolerance on the change in function value
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        FunctionTolerance       
        
%STEPTOLERANCE Termination tolerance on the step size
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        StepTolerance   
        
%JACOBIANMULTIPLYFCN Handle to function that performs Jacobian-vector and
%                    Jacobian-matrix products
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        JacobianMultiplyFcn   
        
%MAXITERATIONS Maximum number of iterations allowed 
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        MaxIterations

%OPTIMALITYTOLERANCE Termination tolerance on the first-order optimality
%                    measure
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        OptimalityTolerance

%SUBPROBLEMALGORITHM Algorithm used to solve a subproblem
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        SubproblemAlgorithm   
                
%TYPICALX Typical x values        
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        TypicalX
        
%LINEARSOLVER Solver used by the "interior-point" algorithm
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.
        LinearSolver 
    end
    
    % Hidden properties
    properties (Hidden, Dependent)
        
%DIAGNOSTICS Display diagnostic information 
%
        Diagnostics
        
%JACOBMULT Function handle for Jacobian multiply function    
%
        JacobMult
        
%MAXITER Maximum number of iterations allowed 
%
        MaxIter
        
%MAXPCGITER Maximum number of PCG (preconditioned conjugate gradient) 
%           iterations        
% 
        MaxPCGIter
        
%PRECONDBANDWIDTH Upper bandwidth of preconditioner for PCG
% 
        PrecondBandWidth
        
%PRESOLVEOPS Operations to perform during presolve.
%
        PresolveOps
        
%TOLCON Tolerance on the constraint violation
% 
        TolCon        
        
%TOLFUN Termination tolerance on the function value
%       
        TolFun
        
%TOLX Termination tolerance on the step size
%       
        TolX
        
%TOLPCG Termination tolerance on the PCG iteration
% 
        TolPCG        
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%          
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%          
        SolverName = 'lsqlin';
    end
    
    
    properties (Hidden, SetAccess = private, GetAccess = public)
        
        % New version property added in third version
        LsqlinVersion
    end
    
      properties(Hidden, Constant, GetAccess=public)
% Constant, globally visible metadata about this class.
% This data is used to spec the options in this class for internal clients
% such as: tab-complete, and the options validation
% Properties
        PropertyMetaInfo = genPropInfo();    
    end
    
    methods (Hidden)
        
        function obj = Lsqlin(varargin)
%Lsqlin Options for LSQLIN
%
%   The OPTIM.OPTIONS.LSQLIN class allows the user to create a set of
%   options for the LSQLIN solver. For a list of options that can be set,
%   see the documentation for LSQLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN creates a set of options for LSQLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(PARAM, VAL, ...) creates a set of options
%   for LSQLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS   
            
            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version; Update property 'LsqlinVersion'
            % instead of superclass property 'Version'.
            obj.Version = 2;
            obj.LsqlinVersion = 7;    
        end
        
    end
    
    % Set/get methods
    methods
        
        function obj = set.MaxPCGIter(obj, value)
            obj = setProperty(obj, 'MaxPCGIter', value);
        end
        
        function obj = set.PrecondBandWidth(obj, value)
            obj = setProperty(obj, 'PrecondBandWidth', value);
        end
        
        function obj = set.TolPCG(obj, value)
            obj = setProperty(obj, 'TolPCG', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.MaxIterations(obj, value)
            obj = setAliasProperty(obj, 'MaxIterations', 'MaxIter', value);
        end        
        
        function obj = set.Display(obj, value)                                                           
            if strcmpi(value, 'testing')
                % Set Display to the undocumented value, 'testing'.
                obj = setPropertyNoChecks(obj, 'Display', 'testing');
            else
                % Pass the possible values that the Display option can take
                % via the fourth input of setProperty.
                obj = setProperty(obj, 'Display', value, ...
                    {'off','none','final', ...
                    'final-detailed','iter','iter-detailed'});
            end

        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.JacobMult(obj, value)
            obj = setProperty(obj, 'JacobMult', value);
        end
        
        function obj = set.JacobianMultiplyFcn(obj, value)
            obj = setAliasProperty(obj, 'JacobianMultiplyFcn', 'JacobMult', value);
        end        
        
        function obj = set.PresolveOps(obj, value)
            obj = setProperty(obj, 'PresolveOps', value);
        end        
        
        function obj = set.TolCon(obj, value)
            obj = setProperty(obj, 'TolCon', value);
        end        
        
        function obj = set.ConstraintTolerance(obj, value)
            obj = setAliasProperty(obj, 'ConstraintTolerance', 'TolCon', value);
        end 
        
        function obj = set.TolFun(obj, value)
            obj = setNewProperty(obj, 'TolFun', value);
        end
        
        function obj = set.FunctionTolerance(obj, value)
            obj = setAliasProperty(obj, 'FunctionTolerance', 'TolFunValue', value);
        end  
        
        function obj = set.OptimalityTolerance(obj, value)
            obj = setAliasProperty(obj, 'OptimalityTolerance', 'TolFun', value);
        end        
        
        function obj = set.TolX(obj, value)
            obj = setNewProperty(obj, 'TolX', value);
        end
        
        function obj = set.StepTolerance(obj, value)
            obj = setAliasProperty(obj, 'StepTolerance', 'TolX', value);
        end        

        function obj = set.SubproblemAlgorithm(obj, value)
            obj = setNewProperty(obj, 'SubproblemAlgorithm', value);
        end        
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
              
        function obj = set.LinearSolver(obj, value)
            obj = setNewProperty(obj, 'LinearSolver', value, obj.PropertyMetaInfo.LinearSolver.Values);
        end
        
        %----------------- Get functions ----------------------------------
        function value = get.ConstraintTolerance(obj)
            value = obj.OptionsStore.Options.TolCon;
        end            
        
        function value = get.StepTolerance(obj)
            value = obj.OptionsStore.Options.TolX;
        end      
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.FunctionTolerance(obj)
            value = obj.OptionsStore.Options.TolFunValue;
        end          
        
        function value = get.JacobMult(obj)
            value = obj.OptionsStore.Options.JacobMult;
        end
        
        function value = get.JacobianMultiplyFcn(obj)
            value = obj.OptionsStore.Options.JacobMult;
        end        
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxIterations(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxPCGIter(obj)
            value = obj.OptionsStore.Options.MaxPCGIter;
        end
        
        function value = get.OptimalityTolerance(obj)
            value = obj.OptionsStore.Options.TolFun;
        end 
        
        function value = get.PrecondBandWidth(obj)
            value = obj.OptionsStore.Options.PrecondBandWidth;
        end
        
        function obj = get.PresolveOps(obj)
            obj = obj.OptionsStore.Options.PresolveOps;
        end        
        
        function value = get.SubproblemAlgorithm(obj)
            value = optim.options.OptionAliasStore.mapOptionFromStore('SubproblemAlgorithm', obj.OptionsStore.Options);
        end
        
        function value = get.TolCon(obj)
            value = obj.OptionsStore.Options.TolCon;
        end        
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
        function value = get.TolPCG(obj)
            value = obj.OptionsStore.Options.TolPCG;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
        function value = get.LinearSolver(obj)
            value = obj.OptionsStore.Options.LinearSolver;
        end
        
    end
    
        % Using mapAlgorithmName to add deprecation check
    methods (Access = protected)
        
        function name = mapAlgorithmName(obj,name)
            %MAPALGORITHMNAME Map old algorithm name to current one
            %
            %   NAME = MAPALGORITHMNAME(OBJ, OLDNAME) maps a previous name for an
            %   algorithm to its current value.
            %
            
            if strcmp(name, 'active-set')
                [linkTag,endLinkTag] = linkToAlgDefaultChangeCsh('lsqlin_warn_will_error'); % links to context sensitive help
                msg = getString(message('optim:quadprog:ActSetRemoved','active-set','lsqlin', ...
                    linkTag,endLinkTag,obj.OptionsStore.AlgorithmNames{:}));
                error('optim:options:QPActSetRemoved',msg);
            end
            
        end
    end    
    
    
    % Hidden utility methods
    methods (Hidden)
        
        function OptionsStruct = mapOptionsForSolver(~, OptionsStruct)
%mapOptionsForSolver Map structure to an optimset one
%
%   OptionsStruct = mapOptionsForSolver(obj, OptionsStruct) maps the
%   specified structure so it can be used in the solver functions and in
%   OPTIMTOOL.            
            % If TolFun is at a default level, set to empty
            if isfield(OptionsStruct, 'TolFun') && ...
                    strcmp(OptionsStruct.TolFun, 'default dependent on problem')
                OptionsStruct.TolFun = [];
                OptionsStruct.TolFunValue = [];
            end
            
        end
        
        function [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
        %mapOptimsetToOptions Map optimset structure to optimoptions
        %
        %   obj = mapOptimsetToOptions(obj, OptimsetStruct) maps specified optimset
        %   options, OptimsetStruct, to the equivalent options in the specified
        %   optimization object, obj.
        %
        %   [obj, OptionsStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
        %   additionally returns an options structure modified with any conversions
        %   that were performed on the options object.
            
            if isfield(OptimsetStruct,'LargeScale') && ~isempty(OptimsetStruct.LargeScale)
                if strcmp(OptimsetStruct.LargeScale, 'on')
                    obj.Algorithm = 'trust-region-reflective';
                else
                    obj.Algorithm = 'interior-point';
                end
            end
            % Also modify the incoming structure.
            if nargout > 1
                OptimsetStruct.Algorithm = obj.Algorithm;
                OptimsetStruct = rmfield(OptimsetStruct,'LargeScale');
            end
        end
        
    end
    
    % Load old objects
    methods (Static = true)
        function obj = loadobj(obj)
            
            % Objects saved in R2013a will come in as structures. 
            if (isstruct(obj) && obj.Version == 1)

                % Save the existing structure
                s = obj;
                
                % Create a new object
                obj = optim.options.Lsqlin;
                
                % Call the superclass method to upgrade the object
                obj = upgradeFrom13a(obj, s); 
                
                % The SolverVersion property was not present in 13a. We
                % clear it here and the remainer of loadobj will set it
                % correctly.
                obj.LsqlinVersion = [];
                
            end
                        
            % Upgrading to 14b
            if (obj.Version < 2)
                % Update OptionsStore by taking the loaded OptionsStore and
                % add info for the interior-point algorithm
                 os = createOptionsStore();
                 os.SetByUser = obj.OptionsStore.SetByUser;
                 os.SetByUser.TolCon = false; 
				 os.SetByUser.PresolveOps = false;
                 os.Options = obj.OptionsStore.Options;
                 os.Options.TolCon = os.AlgorithmDefaults{2}.TolCon; 
                 os.Options.PresolveOps = os.AlgorithmDefaults{2}.PresolveOps;
                 os.AlgorithmIndex = [obj.OptionsStore.AlgorithmIndex false];
                 obj.OptionsStore = os;
            end
            
            % Upgrade to 16a
            if (isempty(obj.LsqlinVersion) || obj.LsqlinVersion < 4)
                % Add TolFunValue
                obj.OptionsStore.AlgorithmDefaults{1}.TolFunValue = 100*eps;
                obj.OptionsStore.IsConstantDefault.TolFunValue = true;
                % Set TolFunValue to whatever of TolFun was saved, but only if the selected algorithm has
                % "FunctionTolerance". Otherwise, set to its default value
                % for another algorithm
                if isfield(obj.OptionsStore.AlgorithmDefaults{obj.OptionsStore.AlgorithmIndex},'TolFunValue') && obj.OptionsStore.SetByUser.TolFun
                    obj.OptionsStore.SetByUser.TolFunValue = obj.OptionsStore.SetByUser.TolFun;
                    obj.OptionsStore.Options.TolFunValue = obj.OptionsStore.Options.TolFun;
                else
                    obj.OptionsStore.SetByUser.TolFunValue = false;
                    obj.OptionsStore.Options.TolFunValue = obj.OptionsStore.AlgorithmDefaults{1}.TolFunValue;
                end
                
                % Objects prior to 15b are missing display-related fields
                % in OptionsStore
                obj.OptionsStore = optim.options.getDisplayOptionFieldsFor16a(...
                    obj.OptionsStore, getDefaultOptionsStore);
            end  
            
            % Upgrade to 17a
            % Default algorithm change and StepTolerance added
            if (isempty(obj.LsqlinVersion) || obj.LsqlinVersion < 5)
                obj.OptionsStore.DefaultAlgorithm = 'interior-point';
                
                % if the default algorithm from previous versions was being
                % used (or if another algorithm was set) we want to
                % update it such that the algorithm is set by user
                if ~strcmpi(obj.OptionsStore.Options.Algorithm, 'interior-point')
                    obj.OptionsStore.SetByUser.Algorithm = true;
                end
                
                % Add StepTolerance and set default to 1e-12 for
                % interior-point
                obj.OptionsStore.AlgorithmDefaults{2}.TolX = 1e-12;
                obj.OptionsStore.IsConstantDefault.TolX = true;
                obj.OptionsStore.SetByUser.TolX = false;
                obj.OptionsStore.Options.TolX = obj.OptionsStore.AlgorithmDefaults{2}.TolX;
            end
            
            % Upgrade to 17b
            % Remove active-set -- mirrors quadprog removal
            if (isempty(obj.LsqlinVersion) || obj.LsqlinVersion < 6)               
                % Create new options store and swap set by user values?
                if obj.OptionsStore.SetByUser.Algorithm && ...
                   strcmpi(obj.OptionsStore.Options.Algorithm,'active-set')
                    % links to context sensitive help
                    [linkTag,endLinkTag] = linkToAlgDefaultChangeCsh('quadprog_warn_will_error'); 
                    warning(message('optim:options:Quadprog:ActiveSetRemovedSwitch',...
                                    obj.OptionsStore.DefaultAlgorithm,linkTag,endLinkTag));
                    obj = obj.resetoptions('Algorithm');
                end
                % remove active-set from OptionsStore
                newOpts = optim.options.Lsqlin;
                
                allOptions = fieldnames(obj.OptionsStore.Options);
                idxTolFunValue = strcmpi(allOptions,'TolFunValue');
                % Code to move TolFunValue if it was set by user
                if obj.OptionsStore.SetByUser.TolFunValue
                   newOpts.FunctionTolerance = obj.FunctionTolerance;
                   % NOTE: this should set the OptionsStore value.   
                end
                % Remove from the list 
                allOptions(idxTolFunValue) = [];

                % Start loop over all options
                for k = 1:numel(allOptions)
                   % NOTE: no try-catch needed now
                   if obj.OptionsStore.SetByUser.(allOptions{k})
                     newOpts.(allOptions{k}) = obj.(allOptions{k});
                   end
                end
                obj = newOpts;
            end
            
            % Upgrade to 18b
            if (isempty(obj.LsqlinVersion) || obj.LsqlinVersion < 7)
                % Add LinearSolver options to 'interior-point' algorithm
                obj.OptionsStore.AlgorithmDefaults{2}.LinearSolver = 'auto';
            end
            
            % Set the version number
            obj.LsqlinVersion = 7;            
            
        end
    end
    
end


function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Lsqlin.

% Define the algorithm names
OS.AlgorithmNames = {'trust-region-reflective', 'interior-point'};

% Define the default algorithm
OS.DefaultAlgorithm = 'interior-point';

% Define the defaults for each algorithm
% trust-region-reflective
OS.AlgorithmDefaults{1}.Diagnostics = 'off';
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.JacobMult = [];
OS.AlgorithmDefaults{1}.MaxIter = 200;
OS.AlgorithmDefaults{1}.MaxPCGIter = 'max(1,floor(numberOfVariables/2))';
OS.AlgorithmDefaults{1}.PrecondBandWidth = 0;
OS.AlgorithmDefaults{1}.TolFun = 100*eps;
OS.AlgorithmDefaults{1}.TolFunValue = 100*eps;
OS.AlgorithmDefaults{1}.TolPCG = 0.1;
OS.AlgorithmDefaults{1}.TypicalX = 'ones(numberOfVariables,1)';

% interior-point
OS.AlgorithmDefaults{2}.Diagnostics = 'off';
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.MaxIter = 200;
OS.AlgorithmDefaults{2}.TolFun = 1e-8;
OS.AlgorithmDefaults{2}.TolCon = 1e-8;
OS.AlgorithmDefaults{2}.TolX = 1e-12;
OS.AlgorithmDefaults{2}.PresolveOps = [];
OS.AlgorithmDefaults{2}.LinearSolver = 'auto';

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS, 'optim.options.Lsqlin');

end

function os = getDefaultOptionsStore

persistent thisos

if isempty(thisos)
    opts = optim.options.Lsqlin;
    thisos = getOptionsStore(opts);
end
    
os = thisos;

end


function propInfo = genPropInfo()
% Helper function to generate constant property metadata for the Lsqlin
% options class.
import optim.internal.TypeInfo

propInfo.Algorithm = TypeInfo.enumType({'interior-point','trust-region-reflective'});
propInfo.ConstraintTolerance = TypeInfo.numericType();
propInfo.Display = TypeInfo.enumType({'off','iter','iter-detailed','final','final-detailed'});
propInfo.FunctionTolerance = TypeInfo.numericType();
propInfo.JacobianMultiplyFcn = TypeInfo.fcnOrEmptyType();
propInfo.MaxIterations = TypeInfo.integerType();
propInfo.OptimalityTolerance = TypeInfo.positiveNumericType();
propInfo.StepTolerance = TypeInfo.positiveNumericType();
propInfo.SubproblemAlgorithm = TypeInfo.enumType({'cg','factorization'});
propInfo.TypicalX = TypeInfo.numericType();
propInfo.LinearSolver = TypeInfo.enumType({'auto','sparse','dense'});
end

