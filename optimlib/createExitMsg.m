function msgNoLinks = createExitMsg(basicMsgArgs,detailedMsgArgs,dispMsg,useDetailedMsg,varargin)
%

%createExitMsg create exit messages for Optimization Toolbox solvers.
%
% This utility creates and displays the exit messages (default and
% detailed) for all Optimization Toolbox solvers.
%
% INPUTS:
% - basicMsgArgs: cell array of arguments for constructing the basic message
% - detailedMsgArgs: cell array of arguments for constructing the basic message
% - dispMsg: a logical scalar which is true when the message is to be displayed in
% the Command Window.
% - useDetailedMsg: a logical scalar which is true when the detailed
% message is to be displayed
%

%   Copyright 2008-2018 The MathWorks, Inc.

% Flag indicating that we will return a message
returnMsg = nargout > 0;
if ischar(dispMsg)
    dispMsg = eval(dispMsg);
end

if ~(returnMsg || dispMsg)
    return
end

% grab basic
basicMsgArgs = msgArgs2Str(basicMsgArgs);
basicMsg = getString(message(basicMsgArgs{:}));
basicMsgNoLinks = stripHTMLTags(basicMsg);
stopCriteriaDetails = '';
detailedMsgNoLinks = '';

% grab detailed (if one exists)
if ~isempty(detailedMsgArgs)
    detailedMsgArgs = msgArgs2Str(detailedMsgArgs);
    detailedMsg = getString(message(detailedMsgArgs{:}));
    detailedMsgNoLinks = stripHTMLTags(detailedMsg);
end

if dispMsg                  % Display Message    
    enableLinks = feature('hotlinks') && ~isdeployed;
    if ~useDetailedMsg
        % Make "stopping criteria details" link if needed
        if enableLinks && ~isempty(detailedMsgArgs)
            % Convert args into strings for the "details" link cmd
            basicStr = string(basicMsgArgs);
            detailStr = string(detailedMsgArgs);
            % Handle "missing" or NaN data
            detailStr(ismissing(detailStr)) = "NaN";
            detailedMsgCmd = char( "createExitMsg({'" + join(basicStr,"','") + "'},{'" + join(detailStr,"','") + "'},true,true);" );
            % Create html tag for the "<detail>" link
            stopCriteriaDetails = getString(message('optimlib:commonMsgs:StopCriteriaDetails',detailedMsgCmd));
            basicMsg = [basicMsg stopCriteriaDetails];
        end
        msg = basicMsg;
        msgNoLinks = basicMsgNoLinks;
    else
        msg = detailedMsg;
        msgNoLinks = detailedMsgNoLinks;
    end
    if enableLinks
        fprintf(msg);
    else
        fprintf(msgNoLinks);
    end
end

% Create message for output structure.
if returnMsg
    % Fetch "<stopping criteria details>" string if not done already
    if ~isempty(detailedMsgNoLinks) && isempty(stopCriteriaDetails)
        stopCriteriaDetails = getString(message('optimlib:commonMsgs:StopCriteriaDetails',''));
    end
    msgNoLinks = [basicMsgNoLinks stripHTMLTags(stopCriteriaDetails) detailedMsgNoLinks];
end

%--------------------------------------------------------------------------
function strNoLinks = stripHTMLTags(str)
strNoLinks = regexprep(str,'</?(\w+).*?>','');

%--------------------------------------------------------------------------
function args = msgArgs2Str(args)

for k = 2:numel(args)
    args{k} = num2str(args{k},'%e');
end
