function q_out = q_product(q1, q2)
    q1z=q1(1);
    q1v=q1(2:4);
    q2z=q2(1);
    q2v=q2(2:4);
    q_out=[q1z*q2z-(q1v)'*q2v; q1z*q2v+q2z*q1v+cross(q1v, q2v)];
end