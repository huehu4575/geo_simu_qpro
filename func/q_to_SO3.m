function R = q_to_SO3(q)
    if isrow(q)
       q=q';
    end
    qv=q(2:4);
    q1=q(2);
    q2=q(3);
    q3=q(4);
    qvh=[0, -q3, q2;
         q3, 0, -q1;
         -q2, q1, 0];
    G=[-(qv), q(1)*eye(3)+qvh];
    H=[-(qv), q(1)*eye(3)-qvh];
    R=G*(H.');
end