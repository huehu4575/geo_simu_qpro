function omega = qomega_to_omega(qo, Ts)
    omega=[0;0;0];
    
    % If qo is a row vector, then transpose
    if isrow(qo)
       qo=qo.';
    end
    
    no_T_half=acos(qo(1)); % =|omega|*Ts/2
    
    % If qo1>1 (because of calculation err), then acos(qo1) will be complex number. So, let qo1=1 and acos(qo1)=0.
    if isreal(no_T_half)==0
       no_T_half = 0;
    end
    
    norm_omega = no_T_half/Ts*2;
    qv=qo(2:4);
    if norm_omega>0
        omega=qv*norm_omega/sin(no_T_half);
    end
end