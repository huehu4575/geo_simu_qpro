function [theta, xi] = q_to_theta_xi(q)
    q1.z=q1(1);
    q1.v=q1(2:4);
    theta=2*acos(q.z);
    if theta==0
     xi=[0;0;0];
    else
     xi=q.v*1/(sin(theta/2));
    end
end