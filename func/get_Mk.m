function Mk = get_Mk(M, k)
    [a,b]=size(M);
    Mk=M(1:a, a*k+1:a*(k+1));
end