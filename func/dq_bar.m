function dq_out = dq_bar(dq1)
    dq_out.p=dq1.p;
    dq_out.d=-dq1.d;
end