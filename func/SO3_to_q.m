function q = SO3_to_q(R)
    theta=acos((trace(R)-1)/2);
    st=sin(theta);
    
    if st>0 || st<0
    xi=1/(2*sin(theta))*[R(3,2)-R(2,3);
                         R(1,3)-R(3,1);
                         R(2,1)-R(1,2)];
    else
        xi=[0;0;0];
    end
    q=[cos(theta/2); xi*sin(theta/2)];
end