function qom = omega_to_qomega(omega)
    global Ts
    qom=[1;0;0;0];
    if isrow(omega)
       omega=omega.';
    end
    no=norm(omega);
    if no>0
        qom(1)=cos(0.5*no*Ts);
        qom(2:4)=omega*(1/no)*sin(0.5*no*Ts);
    end
end