function out = q_norm(q1)
    q1.z=q1(1);
    q1.v=q1(2:4);
    out=sqrt(q1.z*q1.z+(q1.v).'*q1.v);
end