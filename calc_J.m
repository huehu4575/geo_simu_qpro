function [f, g] = calc_J(qa_k, qo_k, bar_qu, Ts, barQa, barQo, barQu, p)

    %% calc {q, omega}_{k+1, ..., k+p}  from q_k, omega_k, u_{k, ..., k+p-1}
    [bar_qa, bar_qo] = calc_state_p_step(qa_k, qo_k, bar_qu, Ts, p);
    
    %% cost function
    bar_qi = repmat([1;0;0;0], p, 1);
    breve_qa = bar_qa - bar_qi;
    breve_qo = bar_qo - bar_qi;
    breve_qu = bar_qu - bar_qi;


    f = 0.5*(breve_qa'*barQa*breve_qa + breve_qo'*barQo*breve_qo + breve_qu'*barQu*breve_qu);
    
    %% grad J
    g = calc_gradJ(bar_qa, bar_qo, bar_qu, qo_k, barQa, barQo, barQu, p, breve_qa, breve_qo, breve_qu);
end
