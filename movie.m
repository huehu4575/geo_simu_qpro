    f1=figure('Position',[100 100 800 600]);

    clf;
az = 0;
el = 90;
view(az, el);
view([0 0]);
    axis equal;
    axis([-2 2 -2 2 -2 2]);
    %axis vis3d;
    grid on;
    yticks([-3:1:3])
    zticks([-3:1:3])
    ax = gca;
    n = Duration/Ts+1;
    for i=1:n
        hcubei{i} = plotcube([1 1.5 0.2], [-0.5 -0.75 -0.1], 0.2);
        ti{i} = hgtransform('Parent',gca);
        cellfun(@(h)set(h,'Parent',ti{i}),hcubei{i});
    end
    hcube = plotcube([1 1.5 0.2], [-0.5 -0.75 -0.1], 0.8);
    ccube = plotcube([0.05 0.05 0.05], [-0.025 -0.025 -0.025], 1.0,[0,0,1]);
    t = hgtransform('Parent',ax);
    cellfun(@(h)set(h,'Parent',t),hcube);
    xlabel('x')
    ylabel('y')
    zlabel('z')
    filename = 'movie_main';
    mov = VideoWriter([strcat('./plot/',graph_prefix,'/',filename) '.avi'], 'Motion JPEG AVI');
    mov.Quality = 100;
    mov.FrameRate = 1/Ts;
    if movie_save==1; open(mov); end
    warning('off','MATLAB:hg:DiceyTransformMatrix')
    text(3.5, 0.5, 1.6, strcat('Sample time=',num2str(Ts)),'FontSize',15);
    text(3.5, 0.5, 1.3, strcat('Pred. horizon=',num2str(p)), 'FontSize',15);
    text(4, 0, 0.1, strcat('Qu=',num2str(qu),'*I'),'FontSize',15);
    text(4, 0, 0.4, strcat('Qo=',num2str(qo),'*I'),'FontSize',15);
    text(4, 0, 0.7, strcat('Qa=',num2str(qa),'*I'),'FontSize',15);
    text(3.5, 0.5, 1.0, strcat('Terminal cost=',num2str(Terminalcost)),'FontSize',15);
    for k=1:n
        t.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        %ti{k}.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        timetext=strcat('t=', num2str(Ts*(k-1), '%10.1f'));
        tx=text(-3.1,3.1,0.5,timetext,'FontSize',22);       
        drawnow;
        if movie_save==1
        frame=getframe(f1);
        %writeVideo(mov,frame);
        end
        %pause
        tx.Visible='off';
    end