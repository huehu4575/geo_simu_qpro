function Hpk = calc_Hpk(q_k)
    qz=q_k(1);
    q1=q_k(2);
    q2=q_k(3);
    q3=q_k(4);
    Hpk=[+qz, -q1, -q2, -q3;
         +q1, +qz, -q3, +q2;
         +q2, +q3, +qz, -q1;
         +q3, -q2, +q1, +qz];
end