%% RAPID
clear; close all; addpath('./func','./test_mfiles','./plot','./optimlib');

%% simulation settings
specify_grad=1; % 0:false, 1:true(faster)
state_update=1; % 0:same as prediction model, 1:use ode45(recommended)
dcheck      =0; % Derivative check
iter_display=0; % iter display
norm_constr =0; % norm constraint

%% output settings
graph_prefix = 'qpro_specify';
movie_output = 1; % 0:skip, 1:output
movie_save   = 1; % 0:ignore, 1:save
graph_save   = 1; % 0:ignore, 1:save


%% parameters
Duration = 4; % simulation time
qa=1; % weight w.r.t. orientation
qo=1; % weight w.r.t. ang velocity
qu=1; % weight w.r.t. inputs
k0 = 3; % max input norm
k1 = 2; % max u1 norm
k2 = 2; % max u2 norm
k3 = 2; % max u3 norm


%% set global variable
%global Qa Qo Qu
Ts = 0.1;             % sample time step
p=5;                 % predict step number
Qa = diag([qa qa qa qa]); % Weighting Matrix of orientation (which represented by quaternion)
Qo = diag([qo qo qo qo]);   % Weighting Matrix of body ang velocity
Qu = diag([qu qu qu qu]);    % Weighting Matrix of inputs
Terminalcost=1;
dim4N = 4*p;      % = 4p : size of input row (4*N=sum{dim(u_1)+dim(u_2)+...dim(u_N)})
barQa = zeros(4*p);        %mbr^{4p*4p}
barQo = zeros(4*p);        %mbr^{4p*4p}
barQu = zeros(4*p);        %mbr^{4p*4p}
for i = 1:p
    barQa(i*4-3:i*4,i*4-3:i*4)=Qa;  % barQa =diag([Qa, Qa, ..., Qa])
    barQo(i*4-3:i*4,i*4-3:i*4)=Qo;  % barQo =diag([Qo, Qo, ..., Qo])
    barQu(i*4-3:i*4,i*4-3:i*4)=Qu;  % barQu =diag([Qu, Qu, ..., Qu])
end
% add terminal cost
barQa(dim4N-3:dim4N,dim4N-3:dim4N) = Terminalcost*barQa(dim4N-3:dim4N,dim4N-3:dim4N);

%% set initial state
R0 = rot_y(-5*pi/6)*rot_z(3*pi/4); % initial orientation (in SO3)
qa0 = SO3_to_q(R0);                % initial orientation (in quaterinon)
qo0 = [1; 0; 0; 0];                 % initial body ang velocity (in quaternion)
qu0 = [1; 0; 0; 0];                % initial inputs
bar_qu=repmat(qu0, p, 1);

%% prepare history
n = Duration/Ts+1;
R_his    = [R0];
qa_his    = repmat(qa0, 1, n);
qo_his    = repmat(qo0, 1, n);
qu_his    = repmat(qu0, 1, n);
t_History = [];
fval_History = [];
grad_his=[];
hess_his=[];

%% simulate
if dcheck == 0; dcheck = 'off'; elseif dcheck ==1; dcheck = 'on'; end
if specify_grad == 0; specify_grad = false; elseif specify_grad ==1; specify_grad = true; end
if iter_display == 0; iter_display = 'off'; elseif iter_display ==1; iter_display = 'iter'; end
options = optimoptions('fmincon','Display', iter_display, 'SpecifyObjectiveGradient', specify_grad,...
          'SpecifyConstraintGradient', specify_grad,'FiniteDifferenceType','central', 'DerivativeCheck', dcheck); 

fprintf('Simulation started.  It might take a while...\n')
A = []; b = []; Aeq = []; beq = []; lb=[]; ub=[];
if norm_constr == 1
    lb = repmat([cos(k0*Ts*0.5);-k1/k0*sin(k0*Ts*0.5);-k2/k0*sin(k0*Ts*0.5);-k3/k0*sin(k0*Ts*0.5)],p,1);
    ub = repmat([1.01;k1/k0*sin(k0*Ts*0.5);k2/k0*sin(k0*Ts*0.5);k3/k0*sin(k0*Ts*0.5)],p,1);
end

for ct = 1:(Duration/Ts)
    qa_k =qa_his(1:4, ct);
    qo_k =qo_his(1:4, ct);

    tic;
        
    % Nonlinear MPC computation with full state feedback (no state estimator)
    COSTFUN = @(bar_qu)calc_J(qa_k, qo_k, bar_qu, Ts, barQa, barQo, barQu, p);
    nonlcon = @(bar_qu)unitsp4(bar_qu, p);

    [bar_qu,fval,exitflag,output,lambda,grad,hessian] = fmincon(COSTFUN, bar_qu, A, b, Aeq, beq, lb, ub, nonlcon, options);
    t_History = [t_History toc];
    
    if state_update==0
        [qa_kp, qo_kp] = calc_state_one_step(qa_k, qo_k, bar_qu(1:4), Ts);
    elseif state_update==1
        [qa_kp, qo_kp] = ContSystem_one_step(qa_k, qo_k, bar_qu(1:4), Ts);
    end
    
    % Save plant states for display.
    grad_his=[grad_his, grad];
    hess_his=[hess_his, hessian];
    fval_History = [fval_History fval];
    qa_his(:, ct+1)    = qa_kp;
    qo_his(:, ct+1)    = qo_kp;
    qu_his(:, ct+1)    = bar_qu(1:4);
    Rkp = q_to_SO3(qa_kp);
    R_his = [R_his; Rkp];
    fprintf('%d/%d\nfval = %d\n', ct, Duration/Ts, fval);
end
fprintf('Simulation finished!\n')
total_J=calc_total_J(n-1, qa_his, qo_his, qu_his, Ts, Qa, Qo, Qu, Terminalcost);

%% Prepare save folder
if graph_save==1 || movie_save==1
    if isempty(dir(strcat('./plot/',graph_prefix)))==0
        fprintf('Overwrite\n');
    end
    mkdir('./plot/', graph_prefix);
end


%% plot result
tspan=0:Ts:Duration;

% calc time plot
f_tictoc=figure('Position', [1900 80 450 400]);
timeavg = sum(t_History)/(Duration/Ts)
timemax = max(t_History)
timemin = min(t_History)
timemed = median(t_History)
boxplot(t_History,'Whisker',10)
ylimmax=(fix(timemax*52)+1)/50;
    % Nonlinear MPC computation with full state feedback (no state estimator)
    COSTFUN = @(bar_qu)calc_J(qa_k, qo_k, bar_qu);
ylim([0 ylimmax]); grid on;
tx=text(1.1,timemed,strcat('$$t_{med}=$$',num2str(timemed)),'FontSize',13,'Interpreter','latex');
ylabel('Calculation time [s]', 'FontSize',14, 'Interpreter', 'latex')
title(strcat('Boxplot of calculation time'),...
    'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/boxplot'),'epsc')
end

% calc time plot (num)
f_tictoc2=figure('Position', [1260 80 600 400]);
%plot(tspan(1:end-1)/Ts, t_History)
bar(t_History)
hold on; grid on;
plot(tspan/Ts, repmat(timemed,1,ct+1),'-.','LineWidth',2)
plot(tspan/Ts, repmat(Ts,1,ct+1),'LineWidth',2)
tx=text(35,timemed+0.015,strcat('$$t_{med}=$$',num2str(timemed)),'FontSize',13,'Interpreter','latex');
ylabel('Calculation time [s]', 'FontSize',14, 'Interpreter', 'latex')
title(strcat('Timeplot of calculation time'),...
    'FontSize',14, 'Interpreter', 'latex')
legend({'Calc. time at each step','Median Value $$t_{med}$$','Time Step $$T_s$$'}, 'FontSize',13, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/timeplot'),'epsc')
end

% orientation plot
f_q=figure('Position', [50 80 600 400]);
plot(tspan, qa_his, 'LineWidth',2);
grid on;
legend({'$$q_z$$', '$$q_{v1}$$', '$$q_{v2}$$','$$q_{v3}$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Orientation $$q_a$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_qa'),'epsc')
end

% ang velocity plot
f_omega=figure('Position', [650 80 600 400]);
plot(tspan, qo_his, 'LineWidth', 2)
grid on;
legend({'$$q_{o1}$$', '$$q_{o2}$$', '$$q_{o3}$$', '$$q_{o4}$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Body Abgular Velocity $$q_o$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_qo'),'epsc')
end

% input plot
f_u=figure('Position', [50 600 600 400]);
stairs(tspan, qu_his(1,:), 'LineWidth',1.7); hold on;
stairs(tspan, qu_his(2,:), 'LineWidth',1.7);
stairs(tspan, qu_his(3,:), 'LineWidth',1.7);
stairs(tspan, qu_his(4,:), 'LineWidth',1.7);
grid on;
legend({'$$q_{u1}$$', '$$q_{u2}$$', '$$q_{u3}$$', '$$q_{u4}$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Input Acceleration $$q_u$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_qu'),'epsc')
end

% fval plot
f_fval=figure('Position', [650 600 600 400]);
semilogy(tspan(2:end), fval_History, 'LineWidth',2)
grid on;
legend({'fval'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Value of fval', 'FontSize',14, 'Interpreter', 'latex')
ylim([1.0*10^ (-15) 10^1])
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_fval'),'epsc')
end

% norm plot
f_norm=figure('Position', [1300 600 600 400]);
qa_n = zeros(n,1);
qo_n = zeros(n,1);
qu_n = zeros(n,1);
for i=1:n
    qa_n(i)=qa_his(1:4,i)'*qa_his(1:4,i);
    qo_n(i)=qo_his(1:4,i)'*qo_his(1:4,i);
    qu_n(i)=qu_his(1:4,i)'*qu_his(1:4,i);
end
plot(tspan, qa_n, 'LineWidth', 2); hold on;
plot(tspan, qo_n, 'LineWidth', 2)
plot(tspan, qu_n, 'LineWidth', 2)
grid on;
legend({'$$q_a^2$$', '$$q_o^2$$', '$$q_u^2$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('norm', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_norm'),'epsc')
end

% ang velocity plot
f_om=figure('Position', [1900 600 600 400]);
omega_his = zeros(3,n);
for i=1:n
    omega_his(1:3, i)=qomega_to_omega(qo_his(1:4,i), Ts);
end
plot(tspan, omega_his, 'LineWidth', 2); hold on;
grid on;
legend({'$$\omega_1$$', '$$\omega_2$$', '$$\omega_3$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('ang velocity $$\omega$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_omega'),'epsc')
end

% input plot
f_om=figure('Position', [2550 600 600 400]);
input_his = zeros(3,n);
input_norm_his=zeros(1,n);
for i=1:n
    input_his(1:3, i)=qomega_to_omega(qu_his(1:4,i), Ts);
    input_norm_his(i)=norm(input_his(:,i));
end
plot(tspan, input_his, 'LineWidth', 2); hold on; grid on;
plot(tspan, input_norm_his, 'LineWidth', 2);
legend({'$$u_1$$', '$$u_2$$', '$$u_3$$', '$$\|u\|$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('input $$u$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_u'),'epsc')
end

%total_J plot
f_J=figure('Position', [2500 80 600 400]);
Jm=total_J(end)
plot(tspan, total_J, 'LineWidth',2)
hold on;
grid on;
plot(tspan, repmat(Jm,1,ct+1),'-')
text(0.1,Jm+2,num2str(Jm))
legend({'$$J$$',strcat('$$J_{Max}=$$',num2str(Jm))}, 'FontSize',13, 'Interpreter', 'latex','Location','southeast')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Sum of $$J$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_total_J'),'epsc')
end


%% Output Movie
if movie_output==1
    f1=figure('Position',[100 100 800 600]);
    clf;
    view(3);
    axis equal;
    axis([-2 2 -2 2 -2 2]);
    axis vis3d;
    grid on;
    yticks([-3:1:3])
    zticks([-3:1:3])
    ax = gca;
    n = Duration/Ts+1;
    for i=1:n
        hcubei{i} = plotcube([1 1.5 0.2], [0 0 0], 0.2);
        ti{i} = hgtransform('Parent',gca);
        cellfun(@(h)set(h,'Parent',ti{i}),hcubei{i});
    end
    hcube = plotcube([1 1.5 0.2], [0 0 0], 0.8);
    ccube = plotcube([0.05 0.05 0.05], [-0.025 -0.025 -0.025], 1.0,[0,0,1]);
    t = hgtransform('Parent',ax);
    cellfun(@(h)set(h,'Parent',t),hcube);
    xlabel('x')
    ylabel('y')
    zlabel('z')
    filename = 'movie_main';
    mov = VideoWriter([strcat('./plot/',graph_prefix,'/',filename) '.avi'], 'Motion JPEG AVI');
    mov.Quality = 100;
    mov.FrameRate = 1/Ts;
    if movie_save==1; open(mov); end
    warning('off','MATLAB:hg:DiceyTransformMatrix')
    text(3.5, 0.5, 1.6, strcat('Sample time=',num2str(Ts)),'FontSize',15);
    text(3.5, 0.5, 1.3, strcat('Pred. horizon=',num2str(p)), 'FontSize',15);
    text(4, 0, 0.1, strcat('Qu=',num2str(qu),'*I'),'FontSize',15);
    text(4, 0, 0.4, strcat('Qo=',num2str(qo),'*I'),'FontSize',15);
    text(4, 0, 0.7, strcat('Qa=',num2str(qa),'*I'),'FontSize',15);
    text(3.5, 0.5, 1.0, strcat('Terminal cost=',num2str(Terminalcost)),'FontSize',15);
    for k=1:n
        t.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        %ti{k}.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        timetext=strcat('t=', num2str(Ts*(k-1), '%10.1f'));
        tx=text(-3.1,3.1,0.5,timetext,'FontSize',22);       
        drawnow;
        if movie_save==1
            frame=getframe(f1);
            writeVideo(mov,frame);
        end
        %pause
        tx.Visible='off';
    end
    if movie_save==1; close(mov); end
    %close all;
end

if graph_save==1
    save(strcat('./plot/',graph_prefix,'/variables'))
end